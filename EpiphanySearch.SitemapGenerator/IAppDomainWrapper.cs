﻿namespace EpiphanySearch.SitemapGenerator
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Custom wrapper for AppDomain to enable painless mocking.
    /// </summary>
    public interface IAppDomainWrapper
    {
        /// <summary>
        /// Finds the specified assembly.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <returns></returns>
        bool FindAssembly(string assembly);
    }
}