﻿namespace EpiphanySearch.SitemapGenerator
{
    public interface IConfig
    {
        bool SeoMetadataInstalled { get; }
    }
}