﻿namespace EpiphanySearch.SitemapGenerator
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Web;

    public class HttpContextWrapper : IHttpContextWrapper
    {
        public string GetServerVariable(string serverVariable)
        {
            return HttpContext.Current.Request.ServerVariables[serverVariable];
        }
    }
}
