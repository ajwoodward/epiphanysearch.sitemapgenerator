﻿namespace EpiphanySearch.SitemapGenerator
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Umbraco.Core.Models;
    using Umbraco.Web;
    
    public class SitemapGenerator
    {
        private readonly IConfig _config;
        private readonly IHttpContextWrapper _httpContext;
        private StringBuilder _sb;

        public SitemapGenerator(IConfig config, IHttpContextWrapper httpContext)
        {
            _config = config;
            _httpContext = httpContext;
            _sb = new StringBuilder();
        }

        /// <summary>
        /// Lists the child nodes, and any child nodes they may have.
        /// </summary>
        /// <param name="publishedContent">Published content node.</param>
        /// <returns></returns>
        public string ListChildNodes(IPublishedContent publishedContent)
        {
            foreach (var node in publishedContent.Children)
            {
                if (!node.Url.Contains("sitemap"))
                {
                    if (node.TemplateId > 0)
                    {
                        _sb.AppendLine("<url>");
                        _sb.AppendLine($"<loc>{GetUrlWithDomainPrefix(node.Url)}</loc>");
                        _sb.AppendLine($"<lastmod>{node.UpdateDate:s}+00:00</lastmod>");
                        _sb.AppendLine("</url>");
                    }

                    if (node.Level <= 100 && node.Children.Count() > 0)
                    {
                        ListChildNodes(node);
                    }
                }
            } 
            return _sb.ToString().Trim('\n');            
        }

        /// <summary>
        /// Prefixes the URL with the current domain.
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <returns></returns>
        private string GetUrlWithDomainPrefix(string url)
        {
            if (url.StartsWith("/"))
                url = url.Substring(1);

            var domainPrefix = $"http://{_httpContext.GetServerVariable("HTTP_HOST")}/";

            if (url.StartsWith(domainPrefix))
                return url;
            else
                return domainPrefix + url;
        }
    }
}