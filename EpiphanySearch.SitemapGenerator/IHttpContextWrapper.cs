﻿namespace EpiphanySearch.SitemapGenerator
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Custom wrapper for HttpContext to enable painless mocking.
    /// </summary>
    public interface IHttpContextWrapper
    {
        /// <summary>
        /// Gets the specified server variable.
        /// </summary>
        /// <param name="serverVariable">The server variable.</param>
        /// <returns></returns>
        string GetServerVariable(string serverVariable);
    }
}