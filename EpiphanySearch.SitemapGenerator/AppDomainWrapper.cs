﻿namespace EpiphanySearch.SitemapGenerator
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class AppDomainWrapper : IAppDomainWrapper
    {
        /// <summary>
        /// Finds the specified assembly.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <returns></returns>
        public bool FindAssembly(string assembly)
        {
            return AppDomain.CurrentDomain.GetAssemblies().Any(a => a.FullName.Contains(assembly));
        }
    }
}