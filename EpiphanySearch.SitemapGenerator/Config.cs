﻿namespace EpiphanySearch.SitemapGenerator
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class Config : IConfig
    {
        private readonly IAppDomainWrapper _appDomainWrapper;

        public Config(IAppDomainWrapper appDomainWrapper)
        {
            _appDomainWrapper = appDomainWrapper;
        }

        public bool SeoMetadataInstalled => _appDomainWrapper.FindAssembly("EpiphanySearch.SeoMetadata");
    }
}