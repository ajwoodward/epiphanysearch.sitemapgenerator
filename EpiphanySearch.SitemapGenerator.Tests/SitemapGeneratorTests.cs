﻿namespace EpiphanySearch.SitemapGenerator.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using EpiphanySearch.SitemapGenerator;
    using Moq;
    using NUnit.Framework;
    using Umbraco.Core.Models;

    [TestFixture]
    public class SitemapGeneratorTests
    {
        private Mock<IHttpContextWrapper> _httpContext;
        private Mock<IConfig> _config;

        [Test]
        public void GivenASitemapGeneratorWhenICallListChildNodesIExpectAllLevelNodesToBeReturnInXml()
        {
            var node = new Mock<IPublishedContent>();
            node.Setup(m => m.Children)
                .Returns(() =>
                {
                    var topNode = new Mock<IPublishedContent>();
                    topNode.Setup(m => m.TemplateId).Returns(1);
                    topNode.Setup(m => m.TemplateId).Returns(1);
                    topNode.Setup(m => m.UpdateDate).Returns(new DateTime(2020, 1, 1, 11, 11, 11));
                    topNode.Setup(m => m.Url).Returns("/url/");
                    topNode.Setup(m => m.Children)
                        .Returns(() =>
                        {
                            var childNode = new Mock<IPublishedContent>();
                            childNode.Setup(m => m.TemplateId).Returns(4);
                            childNode.Setup(m => m.TemplateId).Returns(2);
                            childNode.Setup(m => m.UpdateDate).Returns(new DateTime(2020, 1, 1, 11, 20, 11));
                            childNode.Setup(m => m.Url).Returns("/url/child");
                            childNode.Setup(m => m.Children).Returns(new List<IPublishedContent>());
                            return new List<IPublishedContent> { childNode.Object };
                        });

                    return new List<IPublishedContent> { topNode.Object };
                });

            var sitemapGen = new EpiphanySearch.SitemapGenerator.SitemapGenerator(_config.Object, _httpContext.Object);

            var xml = sitemapGen.ListChildNodes(node.Object);

            Assert.True(xml.Contains("2020-01-01T11:11:11+00:00"));
            Assert.True(xml.Contains("2020-01-01T11:20:11+00:00"));
        }

        [Test]
        public void GivenASitemapGeneratorWhenICallListChildNodesIExpectOneTopLevelNodeToBeReturnInXml()
        {
            var node = new Mock<IPublishedContent>();
            node.Setup(m => m.Children)
                .Returns(() =>
                {
                    var topNode = new Mock<IPublishedContent>();
                    topNode.Setup(m => m.TemplateId).Returns(1);
                    topNode.Setup(m => m.Level).Returns(1);
                    topNode.Setup(m => m.UpdateDate).Returns(new DateTime(2020, 1, 1, 11, 11, 11));
                    topNode.Setup(m => m.Url).Returns("/url/");

                    return new List<IPublishedContent> { topNode.Object };
                });

            var sitemapGen = new EpiphanySearch.SitemapGenerator.SitemapGenerator(_config.Object, _httpContext.Object);

            var xml = sitemapGen.ListChildNodes(node.Object);

            Assert.True(xml.Contains("2020-01-01T11:11:11+00:00"));
        }



        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _httpContext = new Mock<EpiphanySearch.SitemapGenerator.IHttpContextWrapper>();
            _httpContext.Setup(m => m.GetServerVariable(It.Is<string>(s => s.Equals("HTTP_HOST"))))
                .Returns("localhost:2038");
            _config = new Mock<IConfig>();
            _config.Setup(m => m.SeoMetadataInstalled).Returns(true);
        }

        [SetUp]
        public void SetUp()
        {
        }
    }
}