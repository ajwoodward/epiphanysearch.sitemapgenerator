﻿namespace EpiphanySearch.SitemapGenerator.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class ConfigTests
    {
        [Test]
        public void GivenAConfigServiceWhenSeoMetadataIsInstalledIExpectToFindIt()
        {
            var appDomainWrapper = new Mock<EpiphanySearch.SitemapGenerator.IAppDomainWrapper>();
            appDomainWrapper.Setup(m => m.FindAssembly("EpiphanySearch.SeoMetadata"))
                .Returns(true);

            var config = new EpiphanySearch.SitemapGenerator.Config(appDomainWrapper.Object);

            Assert.True(config.SeoMetadataInstalled);
        }

        [Test]
        public void GivenAConfigServiceWhenSeoMetadataIsNotInstalledIDontExpectToFindIt()
        {
            var appDomainWrapper = new Mock<EpiphanySearch.SitemapGenerator.IAppDomainWrapper>();
            appDomainWrapper.Setup(m => m.FindAssembly("EpiphanySearch.SeoMetadata"))
                .Returns(false);

            var config = new EpiphanySearch.SitemapGenerator.Config(appDomainWrapper.Object);

            Assert.False(config.SeoMetadataInstalled);
        }
    }
}